#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
//TODO:想法是根据每一组的长度判断最小的长度，并且把数据存放在 qmap<qvector>中
//然后根据字符判断应该发送哪一个，如果有相同的命令需要动态变化的组,需要不断去更新它
//然后可以设置是否随机变化
namespace Ui {
class widget;
}

class widget : public QWidget
{
    Q_OBJECT

public:
    explicit widget(QWidget *parent = nullptr);
    ~widget();

private:
    Ui::widget *ui;
};

#endif // WIDGET_H

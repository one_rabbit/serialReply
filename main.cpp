#include <QApplication>
#ifdef QMLTARGET
#include <QQmlApplicationEngine>
#include <QCoreApplication>
#else
#include "UI/widget.h"
#endif

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
#ifdef QMLTARGET
    QQmlApplicationEngine engine;
    const QUrl url("./UI/main.qml");
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);
    qDebug() << "www qml";
#else
    Widget w;
    w.show();
    qDebug() << "www widget";
#endif

    qDebug() << "wwwtttt";

    return app.exec();
}

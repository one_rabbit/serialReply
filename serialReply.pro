QT       += core gui qml serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
TEST_VALUE = $$getenv(TARGETVALUE)  #Read env
message("env: " + $$TEST_VALUE) #如果需要分别+依赖就一定需要条件编译
contains(TEST_VALUE, qml){
DEFINES+= QMLTARGET
CONFIG += QMLTARGET
}
else{

}

SOURCES += \
    main.cpp \


HEADERS +=


include(UI/UI.pri)
include(serial/serial.pri)
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

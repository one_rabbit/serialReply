#include "serial.h"
#include <QCoreApplication>
#include <QTime>
#include "qcrc.h"
#include <QSerialPortInfo>

serial::serial(QObject *parent)
    : QObject{parent}
{
    m_serial = new QSerialPort(this);
    connect(m_serial,SIGNAL(errorOccurred(QSerialPort::SerialPortError)),
                this, SLOT(serialPortError(QSerialPort::SerialPortError)));
    m_baudRate = 9600;
}

serial::~serial()
{
    if (m_serial)
        m_serial->close();
}

void serial::delayMs(int msec)
{
    QTime time=QTime::currentTime().addMSecs(msec);
    while(QTime::currentTime() < time)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}

void serial::serialPortError(QSerialPort::SerialPortError error)
{
    if (error== QSerialPort::ResourceError || error== QSerialPort::PermissionError)
    {//终止发送，回馈给主 ui信号
        QString portName=m_serial->portName();
        if(m_serial->isOpen())
            m_serial->close();//并且关闭端口
        emit sigSerialInfo(portName + "端口已断开");
    }
}

QStringList serial::searchPort()
{
    QStringList portList;                                                    //存放串口名
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) //筛选可以用的串口
    {
        QSerialPort serialp;
        serialp.setPort(info);
        if (serialp.open(QIODevice::ReadWrite)) {
            portList.append(serialp.portName());
            serialp.close(); //关掉串口
        }
    }
    return portList;
}

#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QSerialPort>

class serial : public QObject
{
    Q_OBJECT
public:
    explicit serial(QObject *parent = nullptr);
    ~serial();

    QStringList searchPort();///< 遍历可用的串口

private:
    QSerialPort *m_serial=nullptr;///< 串口对象
    int m_baudRate;

    void delayMs(int msec);

signals:
    void sigSerialInfo(QString);
private slots:
    void serialPortError(QSerialPort::SerialPortError error);

};

#endif // SERIAL_H

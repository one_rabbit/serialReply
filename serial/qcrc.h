#ifndef QCRC_H
#define QCRC_H
#include <qvector.h>
typedef unsigned char u8;///< uchar
typedef unsigned short u16; ///< ushort
typedef unsigned int u32; ///< uint
/**********************************************
 *@author cj
 *@date 2022/3/23
 *@version 1.0
 *@copyright
***********************************************/

/**
 *@brief 模板类，是对crc校验的多项式、初始值、异或的赋值的泛化
 *@attention 该模板仅仅适用于 crc校验类
 */
template <typename  T>
struct CrcType {
    T Poly;///< 多项式; 原本的值反转eg:8005 --- A001
    T InitValue;///< 初始值 ;不变
    T XorValue;///< 异或的赋值 ;不变
};
/**
 * @brief 对crc8、crc16、crc32的一个总结类，实现了对它们的校验
 */
class  QCRC
{
public:
    QCRC(){

    }
    static QCRC* getInstance(){
        static QCRC* m_instance;
        if(m_instance == nullptr)
            m_instance = new QCRC();
        return m_instance;
    }
public:
    const CrcType<u16> crc_16_MODBUS={0xA001,0xffff,0x0000}; ///< modbus的参数
    const CrcType<u16> crc_16_USB={0xA001,0xffff,0xffff};    ///< usb16的参数
    const CrcType<u16> crc_16_IBM={0xA001,0x0000,0x0000};    ///< IBM16的参数
    /**
     *@brief 对crc16进行校验;这是一个重载函数
     *@param addr uchar* 首地址
     *@param num uchar* 长度
     *@param type 校验类型 @see  crc_16_MODBUS,crc_16_USB,crc_16_IBM
     *@return 长度为2的uchar 数组;顺序已经调换
     *@see crc8,crc32
     */
    QVector<uchar> crc16(u8 *addr, int num,CrcType<u16> type);
    /**
     *@brief 对crc16进行校验;这是一个重载函数
     *@param data 需要校验的数据
     *@param type 校验类型 @see  crc_16_MODBUS,crc_16_USB,crc_16_IBM
     *@return 长度为2的uchar 数组;顺序已经调换
     *@see crc8,crc32
     */
    QByteArray crc16(QByteArray data,CrcType<u16> type);


    const CrcType<u8> crc_8_MAXIM={0x8C,0x00,0x00};///< MAXIM的参数
    const CrcType<u8> crc_8_ROHC={0xE0,0xff,0x00};///< ROHC的参数
    /**
     *@brief 对crc8进行校验;这是一个重载函数
     *@param addr uchar* 首地址
     *@param num uchar* 长度
     *@param type 校验类型 @see  crc_8_MAXIM,crc_8_ROHC
     *@return 长度为1的uchar 数组;顺序已经调换
     *@see crc8,crc32
     */
    QVector<uchar> crc8(u8 *addr, int width,CrcType<u8> type);
    /**
     *@brief 对crc8进行校验;这是一个重载函数
     */
    QByteArray crc8(QByteArray data,CrcType<u8> type);

    const CrcType<u32> crc_32={0xEDB88320,0xFFFFFFFF,0xFFFFFFFF};///< crc_32
    //const CRC_32 crc_32_MPEG={0xEDB88320,0xFFFFFFFF,0x00000000}; 未反转的
    /**
     *@brief 对crc32进行校验;这是一个重载函数
     *@param addr uchar* 首地址
     *@param num uchar* 长度
     *@param type 校验类型 @see  crc_32
     *@return 长度为4的uchar 数组;顺序已经调换
     *@see crc8,crc32
     */
    QVector<uchar> crc32(u8 *addr, int num,CrcType<u32> type);
    QByteArray crc32(QByteArray data,CrcType<u32> type);


    const CrcType<u8> crc_8_ITU={0x07,0x00,0x55};///< crc8未反转的
    /**
     *@brief 对crc8进行校验
     *@param addr uchar* 首地址
     *@param num uchar* 长度
     *@param type 校验类型 @see  crc_8_ITU
     *@return 长度为1的uchar 数组;顺序已经调换
     *@see crc8,crc32
     */
    QByteArray crc8No(u8 *addr, int num,CrcType<u8> type);

    /**
     *  校验和的 数据
     */
    QByteArray CrcSum(QByteArray data);  //需要fromhex
};

inline QVector<uchar> QCRC::crc16(u8 *addr, int num, CrcType<u16> type)
{
    u16 crc=type.InitValue;
    for (;num>0;num--)
    {
        crc=crc^*addr++;
        for (int i=0;i<8;i++)
        {
            if (crc & 0x0001)
                crc=(crc>>1)^type.Poly;
            else
                crc>>=1;
        }
    }
    crc=crc^type.XorValue;
    uchar low=crc/256;
    uchar high=crc%256;
    QVector<uchar> ret;
    ret.append(high);
    ret.append(low);
    return ret;
}

inline QVector<uchar> QCRC::crc8(u8 *addr, int num, CrcType<u8> type)
{
    u8 crc=type.InitValue;

    for (;num>0;num--)
    {
        crc=crc^*addr++;
        for (int i=0;i<8;i++)
        {
            if (crc & 0x01)
                crc=(crc>>1)^type.Poly;
            else
                crc>>=1;
        }
    }
    crc=crc^type.XorValue;
    uchar low=crc/16;
    uchar high=crc%16;
    uchar retC=high*16+low;//反转
    QVector<uchar> ret;
    ret.append(retC);
    return ret;
}

inline QVector<uchar> QCRC::crc32(u8 *addr, int num, CrcType<u32> type)
{
    u32 crc=type.InitValue;

    for (;num>0;num--)
    {
        crc=crc^*addr++;
        for (int i=0;i<8;i++)
        {
            if (crc & 0x00000001)
                crc=(crc>>1)^type.Poly;
            else
                crc>>=1;
        }
    }
    crc=crc^type.XorValue;
    unsigned short low=crc/65536;
    unsigned short high=crc%65536;

    QVector<uchar> ret;
    ret.append(high/256);
    ret.append(high%256);
    ret.append(low/256);
    ret.append(low%256);

    return ret;
}



inline QByteArray QCRC::crc8No(u8 *addr, int num, CrcType<u8> type)
{
    u8 crc=type.InitValue;

    for (;num>0;num--)
    {
        crc=crc^*addr++;
        for (int i=0;i<8;i++)
        {
            if (crc & 0x80)
                crc=static_cast<u8>((crc<<1)^type.Poly);
            else
                crc<<=1;
        }
    }
    crc=crc^type.XorValue;
    QString str = QString::number(crc, 16).toUpper();
    str = str.mid(1, 1) + str.mid(0, 1); //反向
    QByteArray byteArray=QByteArray::fromHex(str.toLatin1());
    return byteArray;

}

inline QByteArray QCRC::crc16(QByteArray data, CrcType<u16> type)
{
    u16 crc=type.InitValue;

    for (int j=0;j<data.size();j++)
    {
        if (static_cast<int>(data.at(j))<0)
            crc=crc^(static_cast<u16>(data.at(j))+256);
        else
            crc=crc^static_cast<u16>(data.at(j));
        for (int i=0;i<8;i++)
        {
            if (crc & 0x0001)
                crc=(crc>>1)^type.Poly;
            else
                crc>>=1;
        }
    }
    crc=crc^type.XorValue;
    QString str = QString::number(crc, 16).toUpper();
    str = str.mid(2, 2) + str.mid(0, 2); //反向
    QByteArray byteArray=QByteArray::fromHex(str.toLatin1());
    return byteArray;
}
inline QByteArray QCRC::crc8(QByteArray data, CrcType<u8> type)
{
    u8 crc=type.InitValue;

    for (int j=0;j<data.size();j++)
    {
        if (static_cast<int>(data.at(j))<0)
            crc=crc^(static_cast<u8>(data.at(j))+256);
        else
            crc=crc^static_cast<u8>(data.at(j));
        for (int i=0;i<8;i++)
        {
            if (crc & 0x01)
                crc=(crc>>1)^type.Poly;
            else
                crc>>=1;
        }
    }
    crc=crc^type.XorValue;
    QString str = QString::number(crc, 16).toUpper();
    str = str.mid(1, 1) + str.mid(0, 1); //反向
    QByteArray byteArray=QByteArray::fromHex(str.toLatin1());
    return byteArray;
}
inline QByteArray QCRC::crc32(QByteArray data, CrcType<u32> type)
{
    u32 crc=type.InitValue;

    for (int j=0;j<data.size();j++)
    {
        if (static_cast<int>(data.at(j))<0)
            crc=crc^(static_cast<u32>(data.at(j))+256);
        else
            crc=crc^static_cast<u32>(data.at(j));
        for (int i=0;i<8;i++)
        {
            if (crc & 0x00000001)
                crc=(crc>>1)^type.Poly;
            else
                crc>>=1;
        }
    }
    crc=crc^type.XorValue;
    QString str = QString::number(crc, 16).toUpper();
    str = str.mid(4, 4) + str.mid(0, 4); //反向
    QByteArray byteArray=QByteArray::fromHex(str.toLatin1());
    return byteArray;
}

inline QByteArray QCRC::CrcSum(QByteArray data)
{
    int sum= 0;
    for (int i=0;i<data.size();i++) {
        if (static_cast<int>(data.at(i))<0)
            sum=sum + static_cast<int>(data.at(i)+256);
        else
            sum=sum + static_cast<int>(data.at(i));
    }
    QString ret=QString("%1").arg(sum,4,16,QChar('0'));
    ret=ret.mid(0,2)+ret.mid(2,2);

    return ret.toLatin1();
}
#endif // QCRC_H

